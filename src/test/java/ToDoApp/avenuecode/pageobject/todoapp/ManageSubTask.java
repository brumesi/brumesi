package qatest.avenuecode.pageobject.todoapp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import qatest.avenuecode.core.selenium.pageobject.BaseWebPage;

/**
 * Page Object da pagina SubTask
 * @author Bruno de Melo Silva
 *
 */
public class ManageSubTask extends BaseWebPage{
	
	private static By MANAGE_SUB_TASKS = By.xpath("//button[@class='btn btn-xs btn-primary ng-binding']");
	private static By DATE = By.id("dueDate");
	private static By BTN_ADD = By.id("add-subtask");
	private static By BTN_REMOVE = By.xpath("//button[contains(text(),'Remove SubTask')]");

	
	public ManageSubTask(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * Botao de Manage SubTasks
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	
	public WebElement manageSubTask() throws Exception{
		return searchElement.findElementBy(MANAGE_SUB_TASKS, "Manage SubTasks");
		
	}
	
	/**
	 * Botao de Manage SubTasks
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	
	public WebElement date() throws Exception{
		return searchElement.findElementBy(DATE, "Data");
		
	}
	
	/**
	 * Botao de ADD subtasks
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	
	public WebElement btnAdd() throws Exception{
		return searchElement.findElementBy(BTN_ADD, "Botao Add");
		
	}
	
	/**
	 * Botao de Remove subtasks
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	
	public WebElement btnRemoveSubTask() throws Exception{
		return searchElement.findElementBy(BTN_REMOVE, "Botao Remove");
		
	}
	
	

}