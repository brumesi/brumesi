package qatest.avenuecode.pageobject.todoapp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import qatest.avenuecode.core.selenium.pageobject.BaseWebPage;

/**
 * Page Object da pagina home do ToDo
 * @author Bruno de Melo Silva
 *
 */
public class HomeToDo extends BaseWebPage{
	
	
	private static By MYTASKS_BAR = By.xpath("//li/a[text()='My Tasks']");
	private static By MYTASKS_BUTTON = By.xpath("//a[@class='btn btn-lg btn-success']");
	private static By BTN_TASK_REMOVE = By.xpath("//button[@class='btn btn-xs btn-danger']");


	public HomeToDo(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	} 
	@Override
	public boolean isDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
	
	/**
	 * Campo nav Bar My Tasks
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	public WebElement barMyTasks() throws Exception {
		
		return searchElement.findElementBy(MYTASKS_BAR, "MyTasks bar");
	}
	
	
	/**
	 * Botao de My Tasks
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	
	public WebElement btnMyTasks() throws Exception {
		
		return searchElement.findElementBy(MYTASKS_BUTTON, "Botao de My Tasks");
	}
	/**
	 * Botao de Remove tasks
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	
	public WebElement btnRemoveTask() throws Exception{
		return searchElement.findElementBy(BTN_TASK_REMOVE, "Botao Remove tasks");
		
	}
	
		
	

}
