package qatest.avenuecode.pageobject.todoapp;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import qatest.avenuecode.core.selenium.pageobject.BaseWebPage;

/**
 * Page Object da pagina de login de autenticação do ToDoApp
 * @author Bruno de Melo Silva
 *
 */
public class LoginToDoApp extends BaseWebPage{
	
	private static By TODO_SIGN_IN = By.xpath("//li/a[text() = 'Sign In']");
	private static By TODO_LOGIN = By.xpath("//input[@id='user_email']");
	private static By TODO_PASSWORD = By.xpath("//input[@id='user_password']");
	private static By TODO_BUTTON = By.xpath("//input[@name='commit']");
	private static By NAME_LOGGED = By.xpath("//a[contains(text(),'Welcome, Bruno de Melo Silva!')]");
	
	
	public LoginToDoApp(WebDriver webDriver) {
		super(webDriver);
	} 
	@Override
	public boolean isDisplayed() {
		return false;
	}
	
	
	/**
	 * Campo de Sign In Todo
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	public WebElement signInToDo() throws Exception {
		
		return searchElement.findElementBy(TODO_SIGN_IN, "Campo de Sign In ToDo");
	}
	
	
	/**
	 * Campo de user name ToDo App
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	public WebElement userTodo() throws Exception {
		
		return searchElement.findElementBy(TODO_LOGIN, "Usuário ToDo");
	}
	
	/**
	 * Campo de password ToDo App
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	public WebElement passwordToDo() throws Exception {
		
		return searchElement.findElementBy(TODO_PASSWORD, "Senha de usuário ToDo");
	}
	
	
	/**
	 * Botão de Sign in
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	public WebElement signIn() throws Exception {
		
		return searchElement.findElementBy(TODO_BUTTON, "Botão de Sign In");
	}
	
	/**
	 * Campo de boas vindas e o nome de usuário
	 * @author Bruno de Melo Silva
	 * @return
	 * @throws Exception
	 */
	
	public WebElement nameLogged() throws Exception {
		
		return searchElement.findElementBy(NAME_LOGGED, "Welcome, Bruno de Melo Silva!");
	}
	
	
	
}
	