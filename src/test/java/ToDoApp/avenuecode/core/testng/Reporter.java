package qatest.avenuecode.core.testng;

import org.testng.ITestContext;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class Reporter {

	@AfterMethod
    public void finishedTest(final ITestContext testContext) {
		Thread.currentThread().setName(testContext.getSuite().getName());

	}
	
	@BeforeMethod
    public void startTest(final ITestContext testContext) {
		Thread.currentThread().setName(testContext.getName());

	}

}
