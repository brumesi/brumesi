package qatest.avenuecode.core.selenium;

public enum BrowserType {
	internetexplorer, firefox, chrome, edge, safari, android, ios
}
