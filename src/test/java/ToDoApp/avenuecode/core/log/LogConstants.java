package qatest.avenuecode.core.log;

public class LogConstants {
	//public static final String PROPERTIES = new Classloader().getClass().getClassLoader().getResource("config/log4j.properties").toString();
	public static final String PROPERTIES = "./log4j.properties";
	public static final String LOGFOLDER = "./logs/";
	public static final String DATEFORMATCOMPLETELOG = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String DATEFORMATCOMPLETE = "MM/dd/yyyy HH:mm:ss";
	public static final String DATEFORMATCOMPLETEFILE = "yyyy_MM_dd HH_mm_ss_SSS";
	
	
	
}
