package qatest.avenuecode.test.TestToDoApp;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import qatest.avenuecode.core.log.LogConstants;
import qatest.avenuecode.core.selenium.platfom.ChromePlatform;
import qatest.avenuecode.core.selenium.platfom.Platform;
import qatest.avenuecode.test.iteracao.LoginIntegradoToDo;
import qatest.avenuecode.test.iteracao.SubTasks;

public class TestToDoAppSubTask {

	final static Logger logger = Logger.getLogger(TestToDoAppCreateTask.class);
	private static WebDriver webDriver;
	private static SubTasks subTasks;

	@BeforeSuite
	public static void iniciate() throws Exception {

		PropertyConfigurator.configure(LogConstants.PROPERTIES);
		Platform todo = ChromePlatform.StartWebDriver();
		webDriver = todo.getLocalWebDriver();
		LoginIntegradoToDo login = new LoginIntegradoToDo(webDriver);
		login.login();

	}

	@AfterSuite
	public static void teardown() {

		webDriver.close();
		webDriver.quit();

	}

	@Test
	public void createSubTask() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask a
		 *         partir da insercao de dados e clicando em enter
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTask("SubTask1", "Task1", "06/23/2019");
	}

	@Test
	public void createSubTaskNull() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask
		 *         sem dados informados no campo SubTask description e clicando
		 *         em enter
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTask("", "Task1", "06/23/2019");
	}
	
	@Test
	public void createSubTaskNullDate() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask
		 *         sem dados informados no campo Data e clicando
		 *         em enter
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTask("SubTask3", "Task1", "");
	}
	
	@Test
	public void createSubTaskNullDateSubTask() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask
		 *         sem dados informados no campo Data e SubTask description e clicando
		 *         em enter
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTask("", "Task1", "");
	}

	@Test
	public void createSubTaskNG() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask
		 *         sem que dados sejam informados e clicando em enter
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTask("", "Task1", "06/23/2019");
	}

	@Test
	public void createSubTaskMax() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask
		 *         com a insercao de 251 caracteres e clicando em enter
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTask(
				"abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!A",
				"Task1", "06/23/2019");
	}

	@Test
	public void createSubTaskBtn() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask a
		 *         partir da insercao de dados e clicando no botao Add
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTaskBtn("SubTask2", "Task2", "06/23/2019");
	}

	@Test
	public void createSubTaskBtnNull() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask
		 *         sem informar dados no campo SubTask description e clicando no
		 *         botao Add
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTaskBtn("", "Task2", "06/23/2019");
	}
	
	@Test
	public void createSubTaskBtnNullDate() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask
		 *         sem informar dados no campo Data e clicando no
		 *         botao Add
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTaskBtn("SubTask4", "Task2", "");
	}
	@Test
	public void createSubTaskBtnNullDateSubTask() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask
		 *         sem informar dados no campo Data e SubTask description e clicando no
		 *         botao Add
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTaskBtn("", "Task2", "");
	}

	@Test
	public void createSubTaskBtnNG() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask
		 *         sem que dados sejam informados e clicando no botao Add
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTaskBtn("", "Task2", "06/23/2019");
	}

	@Test
	public void createSubTaskBtnNGMax() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma subtask
		 *         com a insercao de 251 caracteres e clicando no botao Add
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.newSubTaskBtn(
				"abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!B",
				"Task12", "06/23/2019");
	}
	
	@Test
	public void removeSubTaskBtn() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a exclusao de uma subtask
		 */
		subTasks = new SubTasks(webDriver);
		subTasks.btnRemoveSubTask();
	}

}
