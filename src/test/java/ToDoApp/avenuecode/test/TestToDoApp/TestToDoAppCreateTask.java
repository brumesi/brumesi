package qatest.avenuecode.test.TestToDoApp;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import qatest.avenuecode.core.log.LogConstants;
import qatest.avenuecode.core.selenium.platfom.ChromePlatform;
import qatest.avenuecode.core.selenium.platfom.Platform;
import qatest.avenuecode.core.testng.Reporter;
import qatest.avenuecode.test.iteracao.LoginIntegradoToDo;
import qatest.avenuecode.test.iteracao.MyTasks;


public class TestToDoAppCreateTask extends Reporter {

	final static Logger logger = Logger.getLogger(TestToDoAppCreateTask.class);
	private static WebDriver webDriver;
	private static MyTasks tasks;

	@BeforeSuite
	public static void iniciate() throws Exception {

		PropertyConfigurator.configure(LogConstants.PROPERTIES);
		Platform todo = ChromePlatform.StartWebDriver();
		webDriver = todo.getLocalWebDriver();
		LoginIntegradoToDo login = new LoginIntegradoToDo(webDriver);
		login.login();

	}

	@AfterSuite
	public static void teardown() {

		webDriver.close();
		webDriver.quit();

	}

	@Test
	public void createTask() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma task a
		 *         partir da insercao de dados e clicando em enter
		 */
		tasks = new MyTasks(webDriver);
		tasks.myTaskBar();
		tasks.newTaskField("New Task");

	}
	
	@Test
	public void createTaskNull() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma task a
		 *         sem informar dados e clicando em enter
		 */
		tasks = new MyTasks(webDriver);
		tasks.myTaskBar();
		tasks.newTaskField("");

	}

	@Test
	public void createTaskNG() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma task a
		 *         partir da insercao de dados inválidos e clicando em enter
		 */
		tasks = new MyTasks(webDriver);
		tasks.myTaskBar();
		tasks.newTaskField("Te");
		tasks.newTaskField("T1");
		tasks.newTaskField("T!");
		tasks.newTaskField("1!");

	}

	@Test
	public void createTaskMax() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma task a partir da insercao de dados com 250 carateres e clicando em enter
		 */
		tasks = new MyTasks(webDriver);
		tasks.myTaskBar();
		tasks.newTaskField(
				"abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!");
	}
	
	@Test
	public void createTaskMaxNG() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma task a partir da insercao de dados com 251 carateres e clicando em enter
		 */
		tasks = new MyTasks(webDriver);
		tasks.myTaskBar();
		tasks.newTaskField(
				"abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!A");
	}

	@Test
	public void createTaskBtnNull() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma task sem informar dados e clicando no botao "+"
		 */
		tasks = new MyTasks(webDriver);
		tasks.myTaskBtn();
		tasks.newTaskBtn("");

	}
	
	@Test
	public void createTaskBtn() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma task a partir da insecao de dados e clicando no botao "+"
		 */
		tasks = new MyTasks(webDriver);
		tasks.myTaskBtn();
		tasks.newTaskBtn("New Task Btn");

	}

	@Test
	public void createTaskBtnNG() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma task a partir da insercao de dados inválidos e clicando no botao "+"
		 */
		tasks = new MyTasks(webDriver);
		tasks.myTaskBtn();
		tasks.newTaskBtn("Te");
		tasks.newTaskBtn("T1");
		tasks.newTaskBtn("T!");
		tasks.newTaskBtn("1!");

	}
	
	@Test
	public void createTaskBtnMaxNG() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a criacao de uma task a
		 *         partir da insercao de dados com 251 carateres e clicando no botao "+"
		 */
		tasks = new MyTasks(webDriver);
		tasks.myTaskBtn();
		tasks.newTaskBtn(
				"abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!!!abc123***!A");
	}
	
	@Test
	public void removeTaskBtn() throws Exception {
		/**
		 * @author Bruno de Melo Silva Teste: Validar a exclusao de task 
		 */
		tasks = new MyTasks(webDriver);
		tasks.myTaskBtn();
		tasks.btnRemoveTask();
	}

	
}
