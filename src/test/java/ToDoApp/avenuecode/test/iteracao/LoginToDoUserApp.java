package qatest.avenuecode.test.iteracao;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import qatest.avenuecode.core.selenium.pageobject.BaseWebPage;
import qatest.avenuecode.pageobject.todoapp.LoginToDoApp;
import qatest.avenuecode.test.utils.PropertiesUtil;

public class LoginToDoUserApp extends BaseWebPage {

	
	private LoginToDoApp login;


		
		public LoginToDoUserApp(WebDriver webDriver) {
		super(webDriver);
		login = new LoginToDoApp(webDriver);
		}			
		
		@Override
		public boolean isDisplayed() {
			return false;
		}
		
		/**
		 * Validar autenticação no website do ToDo
		 * @author Bruno Silva
		 * @throws Exception
		 */
		public void iniciaAutenticação  (){
			
			webDriver.navigate().to(PropertiesUtil.getProp("todo_url"));
		}
		
		/**
		 * Validar login no ToDo
		 * @author Bruno Silva
		 * @throws Exception
		 */
		public void preencheLoginToDo() throws Exception {
					
			WebElement loginToDo = login.userTodo();
			command.click(loginToDo);
			command.clear(loginToDo);
			command.send(loginToDo, PropertiesUtil.getProp("todo_login"));	
			command.screenshot();
		}
		
		/**
		 * Validar login no ToDo com senha
		 * @author Bruno Silva
		 * @throws Exception
		 */
		public void preencheLoginSenha() throws Exception {
					
			WebElement senha = login.passwordToDo();
			command.clear(senha);
			command.send(senha, PropertiesUtil.getProp("todo_password"));
			command.screenshot();
		}
		
		
		/**
		 * Campo Sign In
		 * @author Bruno Silva
		 * @throws Exception
		 */
		
		public void fieldSignIn()throws Exception{
			
			WebElement fieldSignIn = login.signInToDo();
			command.click(fieldSignIn);
		}
		
		/**
		 * Botao Sign In
		 * @author Bruno Silva
		 * @throws Exception
		 */
		
		public void btnSignIn()throws Exception{

				WebElement botaoContinuar = login.signIn();
				command.click(botaoContinuar);
				command.screenshot();
				
			}
		
		/** Clicar no campo Welcome
		 * @author Bruno de Melo Silva
		 * @param search
		 * @throws Exception
		 */
		public void welcome() throws Exception {
			WebElement welcom = login.nameLogged();
			command.click(welcom);
			command.screenshot();

		}

		}
		
		
		
	

