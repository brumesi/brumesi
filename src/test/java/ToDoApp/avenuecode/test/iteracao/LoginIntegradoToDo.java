package qatest.avenuecode.test.iteracao;

import org.openqa.selenium.WebDriver;

import qatest.avenuecode.core.selenium.pageobject.BaseWebPage;

public class LoginIntegradoToDo extends BaseWebPage {

	private static LoginToDoUserApp loginToDo;

	public LoginIntegradoToDo(WebDriver webDriver) {
		super(webDriver);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean isDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}

	public void login() throws Exception {

		loginToDo = new LoginToDoUserApp(webDriver);

		loginToDo.iniciaAutenticação();
		loginToDo.fieldSignIn();
		loginToDo.preencheLoginToDo();
		loginToDo.preencheLoginSenha();
		loginToDo.btnSignIn();
		loginToDo.welcome();

	}

}
