package qatest.avenuecode.test.iteracao;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import qatest.avenuecode.core.selenium.pageobject.BaseWebPage;
import qatest.avenuecode.pageobject.todoapp.HomeToDo;


public class MyTasks extends BaseWebPage{
	
	private HomeToDo taskToDo;

	private static By NEW_TASK = By.xpath("//input[@id='new_task']");
	private static By NEW_TASK_BUTTON = By.xpath("//span[@class='input-group-addon glyphicon glyphicon-plus']");
	
	
	public MyTasks(WebDriver webDriver) {
		super(webDriver);
		taskToDo = new HomeToDo(webDriver);
	}

	@Override
	public boolean isDisplayed() {
		// TODO Auto-generated method stub
		return true;
	}


	/** Cria uma nova Task inserindo um valor e clicando em enter
	 * @author Bruno de Melo Silva
	 * @param search
	 * @throws Exception
	 */
	public void newTaskField(String task) throws Exception {
		WebElement newTask = searchElement.findElementBy(NEW_TASK, "Criar uma nova Task");
		command.send(newTask, task);
		command.pressEnter(newTask);
		command.screenshot();

	}
	
	
	
	/** Cria uma nova Task inserindo um valor e clicando no botao "+"
	 * @author Bruno de Melo Silva
	 * @param search
	 * @throws Exception
	 */
	public void newTaskBtn(String taskBtn) throws Exception {
		WebElement newTaskBtn = searchElement.findElementBy(NEW_TASK_BUTTON, "Criar uma nova Task com o botao");
		command.send(newTaskBtn, taskBtn);
		command.pressEnter(newTaskBtn);
		command.screenshot();

	}	
	
	/** Clicar no menu bar My Tasks
	 * @author Bruno de Melo Silva
	 * @param search
	 * @throws Exception
	 */
	public void myTaskBar() throws Exception {
		WebElement myTaskBar = taskToDo.barMyTasks();
		command.click(myTaskBar);
		command.screenshot();

	}
	
	/** Clicar no botao My Tasks
	 * @author Bruno de Melo Silva
	 * @param search
	 * @throws Exception
	 */
	public void myTaskBtn() throws Exception {
		WebElement myTaskBtn = taskToDo.btnMyTasks();
		command.click(myTaskBtn);
		command.screenshot();

	}
	/** Botao de remover uma subtask
	 * @author Bruno de Melo Silva
	 * @param search
	 * @throws Exception
	 */
	public void btnRemoveTask() throws Exception {

		WebElement btnRemove = taskToDo.btnRemoveTask();
		command.screenshot();
		btnRemove.click();
		command.screenshot();		

	}
	
	
	
}
