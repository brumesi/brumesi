package qatest.avenuecode.test.iteracao;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import qatest.avenuecode.core.selenium.pageobject.BaseWebPage;
import qatest.avenuecode.pageobject.todoapp.ManageSubTask;

public class SubTasks extends BaseWebPage {
	
	private ManageSubTask subTaskToDo;

	private static By NEW_SUB_TASK = By.xpath("//input[@id='new_task']");
	private static By NEW_TASK = By.xpath("//input[@id='new_task']");

	
	
	public SubTasks(WebDriver webDriver) {
		super(webDriver);
		subTaskToDo = new ManageSubTask(webDriver);
	}

	@Override
	public boolean isDisplayed() {
		// TODO Auto-generated method stub
		return true;
	}
	
	/** Cria uma nova SubTask inserindo um valor e clicando em enter
	 * @author Bruno de Melo Silva
	 * @param search
	 * @throws Exception
	 */
	public void newSubTask(String subtask, String task, String nascimento) throws Exception {
		WebElement newTask = searchElement.findElementBy(NEW_TASK, "Criar uma nova Task");
		WebElement newSubTask = searchElement.findElementBy(NEW_SUB_TASK, "Criar uma nova SubTask");
		WebElement date = subTaskToDo.date();
		command.send(newTask, task);
		command.pressEnter(newTask);
		command.screenshot();
		command.send(newSubTask, subtask);
		command.send(date, nascimento);
		command.pressEnter(newSubTask);
		command.screenshot();		

	}
	
	/** Cria uma nova SubTask inserindo um valor e clicando no botao Add
	 * @author Bruno de Melo Silva
	 * @param search
	 * @throws Exception
	 */
	public void newSubTaskBtn(String subtask, String task, String nascimento) throws Exception {
		WebElement newTask = searchElement.findElementBy(NEW_TASK, "Criar uma nova Task");
		WebElement newSubTask = searchElement.findElementBy(NEW_SUB_TASK, "Criar uma nova SubTask");
		WebElement date = subTaskToDo.date();
		WebElement btn = subTaskToDo.btnAdd();
		command.send(newTask, task);
		command.pressEnter(newTask);
		command.screenshot();
		command.send(newSubTask, subtask);
		command.screenshot();
		command.send(date, nascimento);
		command.screenshot();	
		btn.click();
		command.screenshot();		

	}
	
	/** Botao de remover uma subtask
	 * @author Bruno de Melo Silva
	 * @param search
	 * @throws Exception
	 */
	public void btnRemoveSubTask() throws Exception {

		WebElement btnRemove = subTaskToDo.btnRemoveSubTask();
		command.screenshot();
		btnRemove.click();
		command.screenshot();		

	}
	
	


}
